package client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.TreeMap;

import javax.security.auth.login.LoginException;

import datastructures.Page;
import jwiki.core.Wiki;
import util.WikiUtil;

/** Client for searching a wiki for keywords.*/
public class WikiSearchClient
{
	/** (#6) Asks the user to input a list of keywords, and then call searchKeyword and findRelevantPages to 
	 * return and output two ordered lists, one that outputs the results of searchKeyword, and the other that 
	 * outputs the results of findRelevantPages.
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scan= new Scanner(System.in);
		System.out.println("Username: ");
		String username = scan.nextLine();
		System.out.println("Password: ");
		String password = scan.nextLine();
		//Wiki wiki.WikiIndexClient(username, password, "http://en.wikiquote.org/"); 
		//having some issues figuring this out. kinda wish this sort of stuff was in CSE205 instead of "frames 101" haha
		System.out.println("Please input the number of keywords you'd like to search for: ");
		int num = scan.nextInt();
		System.out.println("Please input the maximum number of result pages you'd like to receive: ");
		int num2 = scan.nextInt();
		System.out.println("Now, please input the keywords that you would like to search for (hit enter after each one): ");
		String[] strarray = new String[num];
		for(int i = 0; i < num; i++)
		{
		 strarray[i] = scan.nextLine();
		}
		
		System.out.println("Thank you for your patience. Now, please wait while we process your query.");
		//searchKeywords(well shucks, looks like this area's missing some code, num2);
		//findRelevantPages(oh look I need to put something here don't I, num2)
		
		//now I need to find some way to output the results of both of these.
		
	}
}
