package client;

import java.util.ArrayList;
import java.util.List;
import java.util.*;
import datastructures.Page;
import util.WikiUtil;
import java.nio.file.Files;
import java.lang.Object;

/** WikiIndexClient calls WikiUtil methods to index pages from a wiki.*/
public class WikiIndexClient 
{
	WikiUtil wiki;
	
	public WikiIndexClient(String username, String password, String wikiSite)
	{
		wiki = new WikiUtil(username, password, wikiSite);
	}
	
	/** (#1a) indexPages should start with a seed page (e.g., if you wanted to start with the â€œEducationâ€� page, you can call WikiUtil.getPageText(â€œEducationâ€�)), 
	 * and the maximum number of pages you want to index. It should first store the seed page as a page, and then follow all of the links on that page to other pages, 
	 * and store each other linked-to page as a page. That is, the closer pages to the seed page should be indexed first. Your indexing method should stop once you have 
	 * indexed the maximum number of pages.
	 * @param pageSeed
	 * @param maxPages
	 * @return
	 */
	public Object indexPages(String pageSeed, int maxPages)
	{
		//make queue of strings
		Queue<String> q = new LinkedList<String>(); 
		//add seed
		q.add(pageSeed);
		//linkedlist of pages
		LinkedList<Page> l = new LinkedList<Page>();
		//make page count iterator (number of pages indexed)
		int counter = 1;
		while(!q.isEmpty() && counter <= maxPages)
		{
			String str = q.poll();
			Page nextthing = new Page(str, wiki.getPageText(str), wiki.getPageLinks(str));
			l.add(nextthing);
			for()
			{
			//add adjacent(s) to queue	q
			}
			
			
		//use holder variable with linked list somehow?
			
			
			//breadth-first search? going to try to implement this sometime... 
			//Oh, also, this code is (or rather, will be) adapted from the code provided in our textbook/textbook's website
		/* private void bfs(Digraph G, int s) {
		        Queue<Integer> q = new Queue<Integer>();
		        marked[s] = true;
		        distTo[s] = 0;
		        q.enqueue(s);
		        while (!q.isEmpty()) {
		            int v = q.dequeue();
		            for (int w : G.adj(v)) {
		                if (!marked[w]) {
		                    edgeTo[w] = v;
		                    distTo[w] = distTo[v] + 1;
		                    marked[w] = true;
		                    q.enqueue(w);
		                    */
		 counter++;
		}
		//utilize ctrl+spc
	     
	}
	
	/** (#1b) Writes a collection of pages to one or more files in order to save the information.*/
	public void writePagesToFile(Object pages, Object filename)
	{
		/*Charset charset = Charset.forName("US-ASCII");   //why the heck isn't it recognizing Charset, even though I've imported the related packages? 
		
		try (BufferedWriter writer = Files.newBufferedWriter(filename, charset)) {
		    writer.write(l, 0, l.length());
		} catch (IOException x) {
		    System.err.format("IOException: %s%n", x);								//part of this code was adapted from docs.oracle.com's java documentation
		}																			//I love the smell of citations in the morning
	}
	*/
		
		//this section isn't quite ready. well, none of them are, but I'm definitely trying to do better than Assignment 1's fiasco
		//this code make's Frankenstein's monster look handsome. and at least he was functioning properly. lol
}