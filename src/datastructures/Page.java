package datastructures;

import java.util.List;

import util.TextParserUtil;

/** Data structure representing a single wiki page.*/
public class Page {
	
	private String title;
	private List<String> keywords;
	private List<String> links;
	
	public Page(String t, String text, List<String> l)
	{
		setTitle(t);
		setKeywords(TextParserUtil.extractKeywords(text));
		setLinks(l);
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public List<String> getKeywords() {
		return keywords;
	}
	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}
	
	public void setLinks(List<String> links)
	{
		this.links = links;
	}
	
	public List<String> getLinks()
	{
		return links;
	}
	
}
