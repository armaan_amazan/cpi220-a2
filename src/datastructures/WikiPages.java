package datastructures;

/** Should store information about a collection of wiki pages.*/
public class WikiPages 
{
	public WikiPages()
	{
		
	}
	
	/** (#2) Reads in data from a file and stores it as a graph.*/
	public void createGraph(String filename)
	{
		/*for (String line : Files.readAllLines(Paths.get(filename))) {
		    // ...																				//by the way, this particular bit of code came from stackoverflow: http://stackoverflow.com/questions/2788080/reading-a-text-file-in-java
		}*/																						//gotta cite sources, it's the right thing to do
	}
	
	/** (#3) Reads in data from a file and stores it in an index.*/
	public void createIndex(String filename)
	{
		/*for (String line : Files.readAllLines(Paths.get(filename))) {
		    // ...
		}*/
	}
	
	/** (#4) Takes a list of keywords and the maximum number of pages to return, returns an ordered list of pages
	 * that contain those keywords.*/
	public Object searchKeywords(Object keywords, int max)
	{
		return null;
	}
	
	/** (#5) Takes a list of result pages and the maximum number of relevant pages to return, returns an ordered list 
	 * of pages reachable from the results pages.*/
	public Object findRelevantPages(Object results, int max)
	{
		return null;
	}
}
